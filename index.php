<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Formulaire</title>
	<link rel="stylesheet" type="text/css" href="css/index1.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css">

</head>
<body>
<div class="container-fluid ground">
	<div class="row " >
		<div class="col-md-offset-4 col-md-4">
			<h1 class="title h1_ground">Inscrivez-vous sur <br>le formulaire</h1><br>
			<form method="post" action="traitement.php" enctype="multipart/form-data">
				<div class="input-group">
					<span class="input-group-addon span_span_gly">
						<span class="glyphicon glyphicon-user span_gly"></span>
					</span>
					<input type="text" name="nom" class="form-control" placeholder="Nom" required="" id="nom"  pattern="[A-Za-z]{}">	
				</div> <br>
				<div class="input-group">
					<span class="input-group-addon span_span_gly">
						<span class="glyphicon glyphicon-user span_gly"></span>
					</span>
					<input type="text" name="prenom" class="form-control" placeholder="Prenom" required="" id="prenom" pattern="[A-Za-z]{}">
				</div><br>
				<div class="input-group">
					<span class="input-group-addon span_span_gly">
						<span class="fa fa-transgender span_gly"></span>
					</span>
					<select class="form-control" name="sexe" required="">
						<option selected="selected" disabled="" required="" id="sexe">Sexe</option>
						<option >Homme</option>
						<option >Femme</option>
					</select>
				</div><br>
				
				<div class="input-group">
					<span class="input-group-addon span_span_gly">
						<span class="glyphicon glyphicon-time span_gly"></span>
					</span>
					<input type="date" name="date_naissance" class="form-control" required="" id="date">
				</div><br>
				<div class="input-group">
					<span class="input-group-addon span_span_gly">
						<span class="glyphicon glyphicon-globe span_gly"></span>
					</span>
					<select class="form-control" name="pays" required="" id="pays">
						<option  selected="selected" disabled="">Choix du pays</option>
						<option>Cameroun</option>
						<option>Congo</option>
						<option>Algerie</option>
						<option>Canada</option>
						<option>Japon</option>
						<option>Inde</option>
						<option>Tchad</option>
						<option>Cote Ivoire</option>
					</select>
				</div><br>
				
				
				<div class="input-group">
					<span class="input-group-addon span_span_gly">
						<span class="glyphicon glyphicon-phone span_gly" ></span>
					</span>
					<input type="text" name="telephone" class="form-control" placeholder="Numéro de téléphone"  required="" id="telephone" pattern="[0-9]+">
				</div><br>
				<div class="input-group">
					<span class="input-group-addon span_span_gly">
						<span class="glyphicon glyphicon-envelope span_gly"></span>
					</span>
					<input type="email" name="email"  class="form-control" placeholder="Adresse Email" required="" id="email" pattern="[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([_\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})">
				</div><br>
				<div class="input-group">
					<span class="input-group-addon span_span_gly">
						<span class="glyphicon glyphicon-lock span_gly"></span>
					</span>
					<input class="form-control" type="password" name="pass" placeholder="Mot de passe" required="" id="pass">
				</div><br>
				<div class="row  div_profil input-group">
					<div class="input-group"> 
						<span class="input-group-addon span_span_gly">
							<span class="glyphicon glyphicon-picture span_gly"></span>
						</span>
						<input type="file" name="photo" id="photo1" class="form-control span_span_gly" accept="image/*" onchange="loadFile(event)" id="photo" placeholder="photo de profil">
					</div>
						<div class="col-md-offset-4 col-md-4 form-control" style="margin-top: 10px; margin-bottom: 10px; height: 120px; width: 120px; text-align:center; background-color: inherit !important; border:inherit !important;">
                             <img id="pp" style="height: 120px; width: 120px;text-align:center; border-radius: 10%;" />
                      	</div>
					
                 		
                      	
				</div>
				
				<input class=" form-control btn btn-warning"  type="submit" name="" value="Envoyer" id="send">
			</form>
		</div><br>
	</div><br>
</div>
<script type="text/javascript">
    var loadFile = function(event) {
        var profil = document.getElementById('pp');
        profil.src = URL.createObjectURL(event.target.files[0]);
      };
 	

  </script>
</body>
</html>